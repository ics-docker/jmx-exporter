# maven docker image

ESS maven docker image based on the [official docker image](https://hub.docker.com/_/maven/).

See ``README.md`` in the master branch.
