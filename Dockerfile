FROM openjdk:12-alpine

LABEL maintainer="anders.harrisson@esss.se"

ARG JMX_EXPORTER_VERSION=0.12.0

ENV JMX_EXPORTER_PORT=5556

ADD https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_httpserver/${JMX_EXPORTER_VERSION}/jmx_prometheus_httpserver-${JMX_EXPORTER_VERSION}-jar-with-dependencies.jar /jmx_prometheus_httpserver.jar

COPY jmx_prometheus_config.yml /jmx_prometheus_config.yml

CMD java -jar /jmx_prometheus_httpserver.jar ${JMX_EXPORTER_PORT} /jmx_prometheus_config.yml
